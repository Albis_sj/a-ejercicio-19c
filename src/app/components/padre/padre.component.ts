import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {

  constructor() { }
  mensajePadre! : string;
  SecondMessage! : string;



  recibirMensaje ($event: string): void {
    this.mensajePadre = $event;
    console.log(this.mensajePadre);  
  }

  recibirSegundo ($event: string): void {
    this.SecondMessage = $event;
  }

  ngOnInit(): void {
  }
}
