import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styleUrls: ['./hijo.component.css']
})
export class HijoComponent implements OnInit {

  mensaje: any = {
    name: 'alba',
    edad: 22,

  };
  mensaje2: any = {
    name: 'ambar',
    edad: 18
  };


  @Output() EventoMensaje = new EventEmitter<string>();
  @Output() EventoMensaje2 = new EventEmitter<string>();


  constructor() { }

  ngOnInit(): void {
    this.EventoMensaje.emit(this.mensaje);
    this.EventoMensaje2.emit(this.mensaje2)
    
  }

}
